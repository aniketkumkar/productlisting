# README #

# This is Demo **Product Listing** site #

#Features#
* Auto scrolling
* Captcha verification

# Pr-requisite to build & run the project : 
* Java (1.6 or above), Eclipse (with Java EE & Git installed),  Tomcat server (7 or above)

# How to:
* Open eclipse. 
* Go to Git perspective.
* Clone git repository https://aniketkumkar@bitbucket.org/aniketkumkar/productlisting.git to local directory path.
* Right click on 'productlisting' repository --> import projects -->  Import existing project --> Finish.
* Open project in eclipse project explorer.
* Right click on 'productlisting' project --> Export --> Export as WAR file. 
* Put generated WAR file in /apache-tomcat-7.0.70/webapps folder & start tomcat.
* Fire following url in browser http://localhost:8080/productlisting/home.

# References :
* Infinite scroll: jQuery plugin https://github.com/infinite-scroll/infinite-scroll
* Theme: start bootstrap https://startbootstrap.com/
* Captcha : Google reCaptcha.

#