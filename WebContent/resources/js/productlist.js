  function verifyCaptchaCallback()
 {
	var gcaptcha = grecaptcha.getResponse();
	$.ajax({
		   url: "verifyCaptcha",
		   method: "POST",
		   data: (gcaptcha) ? {captchaResponse: gcaptcha}:{},
		   dataType: "json",
		   success : function(response){
			   var blnVeified = false;
			   
			   if(response) {
				   blnVeified = response;
			   }
			   if(true === blnVeified){
				   $('#content').infinitescroll('bind');
				   $('#captchaDiv').dialog( "destroy" );
				   $('#captchaDiv').hide();
				   //$('.loader-large').show();
			   }
		   },
		   error : function(error){
			console.log('Something went wrong');
		   }
		 });
 }
  
var onloadCallback = function() {
	 $('.loader-large').hide();
	grecaptcha.render('captchaDiv', {
        'sitekey' : '6Le-YykTAAAAAJb3HiIC9Db5p0YHyloZXhLxHmBI',
        'callback' : verifyCaptchaCallback,
      });
	 $( "#captchaDiv" ).dialog({
		 resizable: false,
		 draggable: false,
		 dialogClass: "captcha-title",
		 minHeight:"auto",
		 width: 'auto',
		 modal : true,
		 title: "Solve captcha challenge !"
	 }); 
  };

function getProductList(pageSeq,postJson)
{
	setUiBlock('content');
	 var request = $.ajax({
	   url: "getProductList.json/"+pageSize+"/"+pageSeq,
	   method: "POST",
	   data: (postJson) ? postJson:{},
	   dataType: "json",
	   success : function(data){
		   $('#content > div:not(:first)').remove();
		   $('#content').infinitescroll('update', {state: {currPage : 1}});
		   createProducts(data,'content');
		   releaseUiBlock('content'); 
	   },
	   error : function(error){
		console.log('Something went wrong');
		 releaseUiBlock('content'); 
	   }
	 });
	 
}

function setUiBlock(id)
{
	 $('#'+id).block(
			 { 
				 message: '<i class="fa fa-circle-o-notch fa-spin loader-large"></i>',
				 overlayCSS: { 
					 backgroundColor: '#FFF', 
					 opacity:         0.6, 
					 cursor:          'auto' 
				 },
				 css: { 
					 padding:        0, 
					 margin:         0, 
					 width:          '30%', 
					 top:            '300px', 
					 left:           '35%', 
					 // textAlign:      'center', 
					 color:          'none', 
					 border:         'none', 
					 backgroundColor:'none', 
					 cursor:         'pointer' 
				 },
				 centerY: false
			}); 
}

function releaseUiBlock(id)
{
	 $('#'+id).unblock(); 	
}

function getCategoryProducts(elemCategory)
{
	var categoryId = elemCategory.id;
	if(categoryId){
		var jsonData = {};
		
		if('all' !== categoryId)
		jsonData.category = categoryId;
		
		getProductList(1,jsonData);
		
		 $(".active").removeClass("active");
		$(elemCategory).addClass("active");
	}
}
 
function createProductContent(bean)
{
	//Template for product div
	var productContent = 
	$('<div/>', {
	    class: 'thumbnail'
	})
	.append($('<img/>', {
	    class:"img-responsive",
		src: bean.imgSrc
	}))
	.append($('<div/>', {
	    class:"caption-full"
	})
	.append($('#template button#purchase').clone())
	.append($('<h4/>', {
		 class:'pull-right',
	    text: "Rs "+ bean.productAmt+" /-"
	}))
	.append($('<h4/>', {
	    text: bean.productName + "  ( "+bean.categoryDesc+" )"
	}))
	.append($('<p/>', { 
		text : bean.productDesc
	})))
	.append($('#template .ratings').clone());
	
	return productContent;
}

function createProducts(newElements,targetId)
{
	if(null!=newElements && undefined!=newElements){
    	for(var i=0;i<newElements.length;i++) {
    		var  result = createProductContent(newElements[i]);
			$('#'+targetId).append(result);
    	}
	}
}