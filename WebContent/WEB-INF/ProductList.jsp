<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Product List Demo</title>

    <!-- Bootstrap Core CSS -->
    <link href="resources/theme/css/bootstrap.min.css" rel="stylesheet">
     <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="resources/theme/css/font-awesome.min.css" rel="stylesheet">
     <!-- jQuery CSS -->
    <link href="resources/theme/css/jquery-ui.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="resources/theme/css/shop-item.css" rel="stylesheet">
	
    <script src="resources/theme/js/jquery-3.1.0.min.js"></script>
    <script src="resources/theme/js/jquery.infinitescroll.js"></script>
    <script src="resources/theme/js/jquery.blockUI.js"></script>
    <script src="resources/theme/js/jquery-ui.min.js"></script>
    
    <script src="resources/js/productlist.js"></script>
    
    <script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit'></script>
<script>
//Global Variables
pageSize = ${PAGE_SIZE};

 $( document ).ready(function() {
	 
	 //Bind infinite scroll plugin
	 $('#content').infinitescroll({
			navSelector: ".next",
			nextSelector: "div.next a:first",
			itemSelector: "#content .thumbnail",
			contentSelector: "#content",
			debug: false,
			dataType: 'json',
			bufferPx     : 80,
	     	loading: {
	    	img : "",
	    	msg: "",
	    	finishedMsg:"",
	    	msgText:'<i class="fa fa-circle-o-notch fa-spin loader-large"></i>' 
	    	}, 
	    	path: function(page){
	    		var url = "getProductList.json/5/"+page;
	    		var categoryId = $('#categoryList li.active').attr('id')
	    		if(categoryId && 'all'!== categoryId)
	    			url = url + "?category="+categoryId
	    		return url;
	    	},
			 appendCallback	: false, // USE FOR PREPENDING
	    }, function(newElements, data, url){
	    	createProducts(newElements,'content');
	    });
	 	 $('#content').infinitescroll('unbind');
	  
	  //Load initial products
	  getProductList(1);
	  
	 $( "#categoryList li" ).click(function() {
		 getCategoryProducts(this);
		});
});
</script>   
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-productlist-header navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Digital Photo Gallery</a>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Categories</p>
                <ul id="categoryList" class="list-group">
                	 <li id="all" class="list-group-item active">All</li>
	                <c:forEach items="${PRODUCT_CATEGORY}" var="category">
	                	<li id="${category.category_id}" class="list-group-item">
	                		<c:out value="${category.category_desc}"/>
	                	</li>
	                </c:forEach>
                </ul>
            </div>

            <div id="content" class="col-md-9">
	                <div class="thumbnail hidden">
	                </div>
            </div>
           	 <div class="next">
				 <a href="getProductList.json/${PAGE_SIZE}/2"></a>
			</div>
        </div>

    </div>
    <!-- /.container -->

    <div class="container">
        <hr>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Demo Website 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
    <!-- Templates -->
    <div id="template"  class="hidden">
     <div class="ratings">
        <p class="pull-right">3 reviews</p>
         <p>
             <span class="glyphicon glyphicon-star"></span>
             <span class="glyphicon glyphicon-star"></span>
             <span class="glyphicon glyphicon-star"></span>
             <span class="glyphicon glyphicon-star"></span>
             <span class="glyphicon glyphicon-star-empty"></span>
             4.0 stars
         </p>
     </div>
     <button id="purchase" type="button" class="btn btn-success pull-right">Purchase</button>
     </div>
      <!-- reCaptcha dialog -->
		<div id="captchaDiv"></div>
</body>

</html>
