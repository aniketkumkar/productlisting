package com.productlisting.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.productlisting.bean.ProductBean;
import com.productlisting.bo.ProductListBo;

@Controller
public class ProductListController {
	
	ProductListBo productBo = null;
	public static final String url = "https://www.google.com/recaptcha/api/siteverify";
	public static final String secret = "6Le-YykTAAAAAODbLA3hTk3_-DMaGRjT5ugPtXpL";

	public void setProductListBo(ProductListBo productBo) {
		this.productBo = productBo;
	}


	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView getHomePage(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String,Object> model = new HashMap<String, Object>();
		model.put("PAGE_SIZE", Integer.valueOf(5));
		model.put("PRODUCT_CATEGORY",  productBo.getProductCategories());
		return new ModelAndView("ProductList",model);
	}
	
	
	@RequestMapping(value = "/getProductList.json/{total}/{page}", method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json")
	public @ResponseBody
	List<ProductBean> getProductList(HttpServletRequest request,HttpServletResponse response,
			@RequestParam(value="category", required=false) String category,
			@RequestParam(value="filter", required=false) String filter,
			@PathVariable(value="page")  Integer page,
			@PathVariable(value="total")  Integer total)
	{
		List<ProductBean> lstProduct= null;
		
		if(null == total)
			total=5;
		
		if(null == page)
			page = 1;
		
		page=((page-1)*total);  
		
		lstProduct = productBo.getProductList(category,filter,page,total);
		
		 return lstProduct;
	}
	

	@RequestMapping(value = "/verifyCaptcha", method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json")
	public @ResponseBody
	Boolean  verifyCaptcha(@RequestParam(value="captchaResponse", required=false) String gRecaptchaResponse)
	{
		BufferedReader in = null;
		DataOutputStream wr = null;

		if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
			return false;
		}
		
		try{
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String postParams = "secret=" + secret + "&response="
				+ gRecaptchaResponse;

		// Send post request
		con.setDoOutput(true);
		 wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(postParams);
		
		in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		
		String inputLine;
		StringBuffer result = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			result.append(inputLine);
		}
		
		
		// print result
		System.out.println(result.toString());
		
		//parse JSON response and return 'success' value
		ObjectMapper mapper = new ObjectMapper();
		JsonNode actualObj = mapper.readTree(result.toString());
		System.out.println();
		
		return actualObj.get("success").asBoolean();
		
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		finally
		{
			
			try {
				wr.flush();
				wr.close();
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
			

}
