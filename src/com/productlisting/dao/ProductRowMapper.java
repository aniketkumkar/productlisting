package com.productlisting.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.productlisting.bean.ProductBean;

public class ProductRowMapper implements RowMapper {

	@Override
	public Object mapRow(ResultSet rs, int index) throws SQLException {
		
		ProductBean productBean = new ProductBean();
		
		productBean.setProductId(rs.getString("PRODUCT_ID"));
		productBean.setProductName(rs.getString("PRODUCT_NAME"));
		productBean.setProductDesc(rs.getString("PRODUCT_DESC"));
		productBean.setProductCategory(rs.getString("PRODUCT_CATEGORY"));
		productBean.setProductAmt(rs.getBigDecimal("PRODUCT_AMOUNT"));
		productBean.setImgSrc(rs.getString("IMAGE_SOURCE"));
		productBean.setCategoryDesc(rs.getString("CATEGORY_DESC"));
		return productBean;
	}

}
