package com.productlisting.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.productlisting.bean.ProductBean;

public class ProductListDao {

	private JdbcTemplate jdbcTemplateObject;
	 
	public void setDataSource(DataSource dataSource) 
	{
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@SuppressWarnings("unchecked")
	public List<ProductBean> getProductList(String category, String filter,Integer page, Integer total) {
		
		List<Object> lstArgs = new ArrayList<Object>(1);
		List<ProductBean> result =  new ArrayList<ProductBean>(1);
		StringBuilder stQuery = new StringBuilder();
		
		try
	    {
			stQuery.append("SELECT p.*, c.category_desc FROM product_listing p, product_category c  WHERE p.product_category = c.category_id ");
			if(null != category)
			{
				stQuery.append(" AND product_category = ? ");
				lstArgs.add(category);
			}
			if(null != filter)
			{
				stQuery.append(" AND product_name like ? ");
				lstArgs.add("%"+filter+"%");
			}
			stQuery.append(" LIMIT "+page+", "+total);
   
        	result =  jdbcTemplateObject.query(stQuery.toString(), lstArgs.toArray(), new ProductRowMapper());
        }
        catch (Exception e) 
        {
			e.printStackTrace();
		} 
        
	 return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>>  getProductCategories() {
		
		List<Object> lstArgs = new ArrayList<Object>(1);
		List<Map<String,Object>>  result = null;
		StringBuilder stQuery = new StringBuilder();
		
		try
	    {
			stQuery.append("SELECT category_id, category_desc FROM product_category");
   
        	result =  jdbcTemplateObject.queryForList(stQuery.toString(), lstArgs.toArray());
        }
        catch (Exception e) 
        {
			e.printStackTrace();
		} 
        
	 return result;
	}

}
