package com.productlisting.bo;

import java.util.List;
import java.util.Map;

import com.productlisting.bean.ProductBean;
import com.productlisting.dao.ProductListDao;

public class ProductListBo {
	
	private ProductListDao productDao = null;


	public void setProductListDao(ProductListDao productDao) {
		this.productDao = productDao;
	}


	public List<ProductBean> getProductList(String category, String filter, Integer page, Integer total) 
	{
		return productDao.getProductList(category,filter,page,total);
	}
	
	public List<Map<String,Object>> getProductCategories() 
	{
		return productDao.getProductCategories();
	}

}
